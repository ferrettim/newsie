# Newsie

Newsie is a native [Nextcloud News](https://github.com/nextcloud/news) reader application for Ubuntu Touch. Offline reading is supported once you sync with your Nextcloud server.

Feel free to file bug reports in the [issue tracker](https://gitlab.com/ferrettim/newsie/issues). If you just have a quick question, you can message me directly on Telegram, @martinferretti.

This project is a fork of the now defunct MyWebRSS for FirefoxOS.

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/newsie.martinferretti)

## License

Copyright (C) 2019  Martin Ferretti

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
