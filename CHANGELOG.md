# Changelog
All notable changes to this project will be documented in this file. The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.6.6] - 2019-11-16
### Added
 - Implemented function to add password protected feeds ([Resolves #30](https://gitlab.com/ferrettim/newsie/issues/30))
 - Began implementation to view Starred articles within application.
 - Added Font Awesome.

### Changed
 - Replaced podcast audio player with new customizable player.
 - Changed feed title size in menus and added indicator icon to each feed.
 - Added link to Nextcloud News repo in README.
 - Added Telegram contact info to README.

### Fixed
 - Fixed loading image styling.
 - [#31, Cannot scroll menu feeds](https://gitlab.com/ferrettim/newsie/issues/31)
 - [#32, Titlebar disappears when scrolling beyond viewport](https://gitlab.com/ferrettim/newsie/issues/32)

### Removed
 - Removed unnecessary styling elements.

## [1.6.5] - 2019-11-11
### Added
 - Added option in settings to use application in dark mode.
 - Added option in settings to automatically switch to dark mode at night ([Resolves #28](https://gitlab.com/ferrettim/newsie/issues/28)]
 - Added night light option in settings to switch screen color for more pleasant night time viewing.
 - Added a "Check server status" button under server details setting to help users troubleshoot if they cannot access their feeds.
 - Added Lato-Light.ttf
 - Start Newsie maximized when opening app in desktop mode.

### Changed
 - Moved script to convert time hash into time ago format out of the main application and into a side loaded function to clean things up a bit.
 - Changed font to Lato-Light and improved font rendering for better readability. Also, increased font sizes and made articles list titles bold.
 - Changed image used when all articles are read and changed loading image.
 - Changed size of loading and logo images.
 - Updates to offline caching.
 - Formatted changelog to properly conform to [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) guidelines.
 - Updated README and About pages.

### Fixed
 - [#21, Back to top alignment issue](https://gitlab.com/ferrettim/newsie/issues/21)
 - [#22, Mark as read button showing up when there are no articles](https://gitlab.com/ferrettim/newsie/issues/22)
 - [#23, Issue with link colors in dark mode](https://gitlab.com/ferrettim/newsie/issues/23)
 - [#24, Dark mode not active until Settings selected](https://gitlab.com/ferrettim/newsie/issues/24)
 - [#25, Title alignment issue in server settings](https://gitlab.com/ferrettim/newsie/issues/25)
 - [#26, Articles can be difficult to read with Josefine-Slab font](https://gitlab.com/ferrettim/newsie/issues/26)
 - [#27, Back to top button appears without scrolling](https://gitlab.com/ferrettim/newsie/issues/27)

### Removed
 - Removed some functions that are no longer used.
 - Removed link underlining.
 - Removed Josefine-Slab.ttf
 - Removed deprecated styling attributes.
 - Removed icon switches in auto dark mode.

## [1.6.1] - 2019-11-08
### Added
 - Added data saver mode to prevent favicons, article images, iframes and other external content from loading ([Resolves #17](https://gitlab.com/ferrettim/newsie/issues/17)).
 - Added playback speed support to the podcast player ([Resolves #16](https://gitlab.com/ferrettim/newsie/issues/16)).
 - Added additional attributes to the podcast download links.
 - Added scroll to top button to feed and article toolbars ([Resolves #19](https://gitlab.com/ferrettim/newsie/issues/19)).

### Fixed
 - [#20, Feeds with embedded videos open in new Morph Browser window](https://gitlab.com/ferrettim/newsie/issues/20)

### Changed
 - Changed how settings options are saved to local storage.
 - When clicking previous/next article buttons, new default behavior is for the page to automatically scroll to top instead of maintaining scroll position.
 - Fixed a bug with icon rendering so that menu and toolbar icons no longer show up blurry.

### Fixed
 - [#18, Article scroll position persists when moving to previous/next articles in a feed](https://gitlab.com/ferrettim/newsie/issues/18)
 - [#12, Blurry icons](https://gitlab.com/ferrettim/newsie/issues/12)

### Removed
 - Removed unnecessary code after some refactoring.

## [1.5.5] - 2019-11-05
### Added
 - Added favicon in article view ([Resolves #14](https://gitlab.com/ferrettim/newsie/issues/14)).
 - Added loading image while content is loading.

### Changed
 - Improvements to offline caching.
 - Changed settings behavior so that settings are applied automatically instead of after pressing the sync button.
 - Minor UI tweaks.
 - About page now only shows changelog for past five releases. User is directed to this CHANGELOG for information on previous release changes.

### Fixed
- [#9, Changing settings options does not actually change settings](https://gitlab.com/ferrettim/newsie/issues/9)
- [#13, Explore tab not available offline](https://gitlab.com/ferrettim/newsie/issues/13)
- [#11, Setting options do not persist after application restart](https://gitlab.com/ferrettim/newsie/issues/11)
- [#15, Marking items as read does not update the unread counts](https://gitlab.com/ferrettim/newsie/issues/15)

### Removed
 - Removed unnecessary images and styling left over from previous UI iterations.

## [1.5.1] - 2019-11-02
### Added
 - Added option to sort feeds by number of unread articles or by feed name.
 - Added option to display all or only unread feed items.
 - Added option to display all or only unread feeds.
 - Added favicons in the feed and article views.
 - Added button to remove article from starred items.
 - Above changes resolve [#10](https://gitlab.com/ferrettim/newsie/issues/10).

### Changed
 - Article font size to 1.2rem.

## [1.4.5] - 2019-11-01
### Added
 - Added ability to download and play podcasts directly from the application.
 - Added some element filtering for a cleaner reading experience for feeds that try to add iframes and other unnecessary elements.
 - Added indicator for starred articles (adding/removing starred articles not yet implemented).

### Changed
 - Upgraded backboneLocalStorageJS to 1.1.16
 - Upgraded RequireJS to 2.3.6
 - Upgraded textJS to 2.0.16
 - Upgraded ZeptoJS to 1.2.0

### Removed
 - Removed Google JSAPI.

## [1.4.2] - 2019-10-30
### Changed
 - Tweaked how to add news server and new feeds in order to avoid issues caused [Morph Browser #54](https://github.com/ubports/morph-browser/issues/54) and [Morph Browser #239](https://github.com/ubports/morph-browser/issues/239) on OTA-12 and upcoming devel images.
 - Minor UI tweaks.

## [1.4.1] - 2019-10-29
### Added
 - Added offline reading support.

### Changed
 - Moved add feed button to bottom toolbar in menu.
 - Updated README and About page.
 - Further UI refinements.

## [1.3.5] - 2019-10-28
### Added
 - Implemented social sharing buttons
 - Added icons for social sharing feature

### Changed
 - Changed some spelling in release notes.
 - Minor UI tweaks!

## [1.3.1] - 2019-10-27
### Added
 - Added new logo image for application.
 - Added new open article in browser button to the header.

### Changed
 - Changed icon set to Suru icon set to make the app feel a little more at home.
 - Enough UI tweaks to shake a stick at including lots of work to application styling!

### Removed
 - Removed unnecessary styling code.
 - Removed tons of image UI elements as they were converted to CSS instead.

## [1.2.2] - 2019-10-25
### Changed
 - Minor bugfixes to the user interface.
 - More code cleanup.

## [1.2.1] - 2019-10-25
### Added
 - Added Nextcloud icon to settings page.

### Changed
 - Code cleanup
 - Big speed improvements and faster loading times.
 - UI tweaks

### Removed
 - Removed unnecessary icons and UI elements.

## [1.1.1] - 2019-10-24
### Added
 - Added a bit of code to remove some, let's say, "junky" stuff from some feeds.

### Changed
 - Feeds now sort alphabetically.
 - Fixed date formatting issue.
 - Improved user experience and UI changes.

## [1.0.2] - 2019-10-23
### Added
 - Added app icon.

### Changed
 - Changed app font from Open Sans to Josefine Slab.
 - Updates to About page.

## [1.0.1] - 2019-10-22
### Changed
 - Bugfixes and UI tweaks.

## [1.0.0] - 2019-10-21
### Added
- Initial source release.

[UNRELEASED]: https://gitlab.com/ferrettim/newsie/compare/6c5fe336708ccc0a5efe22db800f878c281524df...66c9c1a01c856db11f3426c2edc055fc8461d6b8
