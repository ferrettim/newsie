// Filename: ArticleModel.js

define([
	'backbone'
], function(Backbone) {
	var ArticleModel = Backbone.Model.extend({
		defaults: {
			id: 0,
			guid: "",
			feed: "",
			api: null,
			title: "Default",
			description: "Default Article",
			author: "",
			url: "http://",
			image: "http://",
			date: 0,
			enclosureMime: "",
			enclosureLink: "",
			status: "",
			starred: ""
		}
	});

	return ArticleModel;
});
