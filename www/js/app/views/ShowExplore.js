// Filename: ShowExploreView.js

define([
	'backbone', 'views/Status', 'text!templates/ShowExplore.html'
], function(Backbone, StatusView, ShowExploreTemplate) {
	var ShowExploreView = Backbone.View.extend({
		el: $("#page"),

		initialize: function() {
			// At least one API is needed
			if(!window.apis.length) {
				(new StatusView()).setMessage("You must have a Nextcloud News server connected before adding a feed.");
				window.location = "#settings";
			}

			// Show the buttons
			$("#button-menu").show();
		},

		render: function(){
			$("#page-title").html("Explore");
			this.$el.html(_.template(ShowExploreTemplate, {apis: window.apis.toJSON()}));

			// Add first feed
			$("#addfeed-submit1").click(function(event) {
				event.preventDefault();

				var api = parseInt($("#addfeed-api").val());
				if(api < window.apis.length) {
					$("#addfeed-submit1").attr("disabled", "disabled");

					window.apis.at(api).feed_add($("#addfeed-url1").val(), function(success, id) {
						$("#addfeed-submit1").removeAttr("disabled");

						if(success) {
							if(id > 0)
								window.location = "#feed/" + api + "/" + id;
							else
								window.location = "#";
						}
					});
				}
			});

      // Add second feed
      $("#addfeed-submit2").click(function(event) {
				event.preventDefault();

				var api = parseInt($("#addfeed-api").val());
				if(api < window.apis.length) {
					$("#addfeed-submit2").attr("disabled", "disabled");

					window.apis.at(api).feed_add($("#addfeed-url2").val(), function(success, id) {
						$("#addfeed-submit2").removeAttr("disabled");

						if(success) {
							if(id > 0)
								window.location = "#feed/" + api + "/" + id;
							else
								window.location = "#";
						}
					});
				}
			});

      // Add third feed
      $("#addfeed-submit3").click(function(event) {
				event.preventDefault();

				var api = parseInt($("#addfeed-api").val());
				if(api < window.apis.length) {
					$("#addfeed-submit3").attr("disabled", "disabled");

					window.apis.at(api).feed_add($("#addfeed-url3").val(), function(success, id) {
						$("#addfeed-submit3").removeAttr("disabled");

						if(success) {
							if(id > 0)
								window.location = "#feed/" + api + "/" + id;
							else
								window.location = "#";
						}
					});
				}
			});

      // Add fourth feed
      $("#addfeed-submit4").click(function(event) {
				event.preventDefault();

				var api = parseInt($("#addfeed-api").val());
				if(api < window.apis.length) {
					$("#addfeed-submit4").attr("disabled", "disabled");

					window.apis.at(api).feed_add($("#addfeed-url4").val(), function(success, id) {
						$("#addfeed-submit4").removeAttr("disabled");

						if(success) {
							if(id > 0)
								window.location = "#feed/" + api + "/" + id;
							else
								window.location = "#";
						}
					});
				}
			});

      // Add fifth feed
			$("#addfeed-submit5").click(function(event) {
				event.preventDefault();

				var api = parseInt($("#addfeed-api").val());
				if(api < window.apis.length) {
					$("#addfeed-submit5").attr("disabled", "disabled");

					window.apis.at(api).feed_add($("#addfeed-url5").val(), function(success, id) {
						$("#addfeed-submit5").removeAttr("disabled");

						if(success) {
							if(id > 0)
								window.location = "#feed/" + api + "/" + id;
							else
								window.location = "#";
						}
					});
				}
			});

		}
	});

	return ShowExploreView;
});
