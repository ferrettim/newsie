// Filename: SettingsView.js

define([
	'backbone', 'views/Status', 'text!templates/Settings.html',
	'models/APINextCloud'
], function(Backbone, StatusView, SettingsTemplate, APINextCloudModel) {
	var SettingsView = Backbone.View.extend({
		el: $("#page"),

		initialize: function() {
			this.template = SettingsTemplate;

			// Show the buttons
			$("#button-menu").show();
		},

		render: function(){
			$("#page-title").html("Settings");
			this.$el.html(_.template(this.template, {apis: window.apis.toJSON()}));

			// Auto-refresh
			$("#check-autorefresh").prop("checked", $.localStorage("autorefresh"));

			$("#check-autorefresh").click(function() {
				$.localStorage("autorefresh", $("#check-autorefresh").is(":checked"));

				// Disable auto-refresh
				if(!$.localStorage("autorefresh")) {
					if(window.autorefresh_cnt)
						clearTimeout(window.autorefresh_cnt), window.autorefresh_cnt = 0;
				}
				else {
					$("#menu-refresh").click();
					window.refresh_all();
				}
			});

			// Show only unread feed items
			$("#check-onlyunread").prop("checked", $.localStorage("onlyunread"));

			$("#check-onlyunread").click(function() {
				$.localStorage("onlyunread", $("#check-onlyunread").is(":checked"));
				// document.getElementById('home').click();
				document.location.reload();

				// Disable only unread
				if($.localStorage("onlyunread")) {
					if(window.onlyunread_cnt)
						clearTimeout(window.onlyunread_cnt), window.onlyunread_cnt = 0;
					// document.getElementById('home').click();
					document.location.reload();
				}
			});

			// Show only unread feeds
			$("#check-onlyunreadfeeds").prop("checked", $.localStorage("onlyunreadfeeds"));

			$("#check-onlyunreadfeeds").click(function() {
				$.localStorage("onlyunreadfeeds", $("#check-onlyunreadfeeds").is(":checked"));
				document.location.reload();

				// Disable only unread
				if(!$.localStorage("onlyunreadfeeds")) {
					if(window.onlyunreadfeeds_cnt)
						clearTimeout(window.onlyunreadfeeds_cnt), window.onlyunreadfeeds_cnt = 0;
					document.location.reload();
				}
			});

			// Sort feeds by unread items
			$("#check-byunread").prop("checked", $.localStorage("byunread"));

			$("#check-byunread").click(function() {
				$.localStorage("byunread", $("#check-byunread").is(":checked"));
				document.location.reload();

				// Disable sorting by unread
				if(!$.localStorage("byunread")) {
					if(window.byunread_cnt)
						clearTimeout(window.byunread_cnt), window.byunread_cnt = 0;
					document.location.reload();
				}
			});

			// Enable low data
			$("#check-lowdata").prop("checked", $.localStorage("lowdata"));

			$("#check-lowdata").click(function() {
				$.localStorage("lowdata", $("#check-lowdata").is(":checked"));
				document.location.reload();

				// Disable low data
				if(!$.localStorage("lowdata")) {
					if(window.lowdata_cnt)
						clearTimeout(window.lowdata_cnt), window.lowdata_cnt = 0;
					document.location.reload();
				}
			});

			// Enable dark mode
			$("#check-darkmode").prop("checked", $.localStorage("darkmode"));

			$("#check-darkmode").click(function() {
				$.localStorage("darkmode", $("#check-darkmode").is(":checked"));
					$.localStorage("autodark", false);
					document.location.reload();

				// Disable dark mode
				if(!$.localStorage("darkmode")) {
					if(window.darkmode_cnt)
						clearTimeout(window.darkmode_cnt), window.darkmode_cnt = 0;
						document.location.reload();
				}
			});

			// Enable auto dark
			$("#check-autodark").prop("checked", $.localStorage("autodark"));

			$("#check-autodark").click(function() {
				$.localStorage("autodark", $("#check-autodark").is(":checked"));
				$.localStorage("darkmode", false);
				document.location.reload();

				// Disable auto dark
				if(!$.localStorage("autodark")) {
					if(window.autodark_cnt)
						clearTimeout(window.autodark_cnt), window.autodark_cnt = 0;
						document.location.reload();
				}
			});

			// Enable nigh light
			$("#check-nightlight").prop("checked", $.localStorage("nightlight"));

			$("#check-nightlight").click(function() {
				$.localStorage("nightlight", $("#check-nightlight").is(":checked"));
				document.location.reload();

				// Disable auto dark
				if(!$.localStorage("nightlight")) {
					if(window.nightlight_cnt)
						clearTimeout(window.nightlight_cnt), window.nightlight_cnt = 0;
						document.location.reload();
				}
			});

			// Enable nigh light
			$("#check-linknewwindow").prop("checked", $.localStorage("linknewwindow"));

			$("#check-linknewwindow").click(function() {
				$.localStorage("linknewwindow", $("#check-linknewwindow").is(":checked"));
				document.location.reload()

				// Disable auto dark
				if(!$.localStorage("linknewwindow")) {
					if(window.linknewwindow_cnt)
						clearTimeout(window.linknewwindow_cnt), window.linknewwindow_cnt = 0;
						document.location.reload();
				}
			});

			// Add an API
			$("#addapi-api").change(function() {
				// Hide or show the URL
				var api = $("#addapi-api").val();
				var url = "none";
				if(api == "nextcloud")
					url = "https://";

				// Show the URL only it is needed
				if(url != "none") {
					$("#addapi-url > input").val(url);
					$("#addapi-url").show();
				}
				else {
					$("#addapi-url > input").val(url);
					$("#addapi-url").hide();
				}
			});

			$("#addapi-submit").click(function(event) {
				event.preventDefault();
				var status = new StatusView();

				var short_name = $("#addapi-api").val();
				var url = $("#addapi-url > input").val();

				if(short_name == "---")
					return status.setMessage("Please select a valid server first");

				// Select the good API model
				if(short_name == "nextcloud")
					var api = new APINextCloudModel();
				else
					return status.setMessage("Can't find a valid handler for this server");

				// Check the URL
				if(!url)
					return status.setMessage("Please enter a URL");
				// Remove trailing slash
				if(url.substr(-1) == '/') {
					url = url.substr(0, url.length-1);
				}
				api.attributes.url = url;

				if(window.apis.where({ url: url }).length)
					return status.setMessage("This address is already in use!");

				// Add it to the collection
				window.apis.add(api);
				window.apis.save();
				window.location = "#api/" + (window.apis.length-1);
			});
		}
	});

	return SettingsView;
});
