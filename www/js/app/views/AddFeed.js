// Filename: AddFeedView.js

define([
	'backbone', 'views/Status', 'text!templates/AddFeed.html'
], function(Backbone, StatusView, AddFeedTemplate) {
	var AddFeedView = Backbone.View.extend({
		el: $("#page"),

		initialize: function() {
			// At least one API is needed
			if(!window.apis.length) {
				(new StatusView()).setMessage("You must have a Nextcloud News server connected before adding a feed.");
				window.location = "#settings";
			}

			// Show the buttons
			$("#button-menu").show();
		},

		render: function(){
			$("#page-title").html("Add Feed");
			this.$el.html(_.template(AddFeedTemplate, {apis: window.apis.toJSON()}));

			// Add a feed with an URL
			$("#addfeed-submit").click(function(event) {
				event.preventDefault();

				var api = parseInt($("#addfeed-api").val());
				if(api < window.apis.length) {
					$("#addfeed-submit").attr("disabled", "disabled");

					window.apis.at(api).feed_add($("#addfeed-url").val(), function(success, id) {
						$("#addfeed-submit").removeAttr("disabled");

						if(success) {
							if(id > 0)
								window.location = "#feed/" + api + "/" + id;
							else
								window.location = "#";
						}
					});
				}
			});
		}
	});

	return AddFeedView;
});
