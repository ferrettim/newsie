// Filename: ArticleView.js

define([
	'backbone', 'views/Status', 'text!templates/Article.html', 'models/Article'
], function(Backbone, StatusView, ArticleTemplate, ArticleModel) {
	var ArticleView = Backbone.View.extend({
		el: $("#sidepage"),
		article: null,
		api: null,
		feed: null,
		model: new ArticleModel(),

		initialize: function() {
			this.template = ArticleTemplate;

			// Show the buttons
			$("#button-back").show();
			$("#button-previous").show();
			$("#button-web").show();
			$("#button-share").show();
			$("#button-next").show();
			$("#button-top").show();
			$("#button-star").show();

			// Back to the feed
			var view = this;
			$("#button-back").click(function() {
				if(view.feed != 0)
					window.location = "#feed/" + view.api + "/" + view.feed;
				else
					window.location = "#";
			});
		},

		render: function(){
			$("#button-previous").attr("disabled", "disabled");
			$("#button-next").attr("disabled", "disabled");

			// Change the content
			if (this.model.attributes.starred === true) {
				$("#sidepage-title").html("");
			  $("#button-star").html("<span class='icon icon-star-off'>unstar</span>");
			} else {
				$("#sidepage-title").html("");
				$("#button-star").html("");
				this.$el.html(_.template(this.template, this.model.toJSON()));
			}

			// Remove iframes
			var iframes = document.getElementsByTagName('iframe');
			for (var i = 0; i < iframes.length; i++) {
				iframes[i].parentNode.removeChild(iframes[i]);
			}
			var containers = document.getElementsByClassName('video-container');
			for (var j = 0; j < containers.length; j++) {
				containers[j].parentNode.removeChild(containers[j]);
			}
			if($.localStorage("lowdata")) {
				var images = document.getElementsByTagName('img');
				for (var k = 0; k < images.length; k++) {
					images[k].parentNode.removeChild(images[k]);
				}
			}
			function countWords(){
				s = this.article.attributes.description.value;
				s = s.replace(/(^\s*)|(\s*$)/gi,"");
				s = s.replace(/[ ]{2,}/gi," ");
				s = s.replace(/\n /,"\n");
				document.getElementById("wordcount").value = JSON.parse(s).split(' ').length;
			}

			// Handle the previous and next buttons
			if(window.articles && this.article) {
				var view = this;

				// Get the current ID in the array
				for(var i = 0; i < window.articles.length; i++) {
					if(window.articles.at(i).attributes.id == this.article)
						break;
				}

				// Previous button
				if(i > 0) {
					$("#button-previous").click(function() {
						window.location = "#feed/" + view.api + "/" + view.feed + "/" + window.articles.at(i-1).attributes.id;
						document.getElementById("top").scrollIntoView({ behavior: "auto", block: "nearest", inline: "start" });
					});
					$("#button-previous").removeAttr("disabled", "");
				}

				// Back to top button
				$("#button-top").click(function() {
					document.getElementById("top").scrollIntoView({ behavior: "auto", block: "nearest", inline: "start" });
				});

				// Next button
				if(i < window.articles.length-1) {
					$("#button-next").click(function() {
						window.location = "#feed/" + view.api + "/" + view.feed + "/" + window.articles.at(i+1).attributes.id;
						console.log(window.articles.at(i+1).attributes.id);
						document.getElementById("top").scrollIntoView({ behavior: "auto", block: "nearest", inline: "start" });
					});
					$("#button-next").removeAttr("disabled", "");
				}
			}

			// Open the links in the browser
			$("#sidepage a").attr("target", "_blank");
		},

		loadArticle: function(api, feed, id) {
			this.article = id;
			this.api = api;
			this.feed = feed;

			if(api >= window.apis.length)
				return;

			if(!window.articles || !this.article) {
				$("#button-back").click();
				return;
			}

			if(!window.articles || !this.article) {
				$("#button-web").click();
				return;
			}

			if(!window.articles || !this.article) {
				$("#button-share").click();
				return;
			}

			if(!window.articles || !this.article) {
				$("#button-star").click();
				return;
			}

			// Get the current ID in the array
			for(var i = 0; i < window.articles.length; i++) {
				if(window.articles.at(i).id == this.article)
					break;
			}

			// Set the article
			if((i >= 0) && (i < window.articles.length)) {
				delete this.model;
				this.model = window.articles.at(i);
				this.render();
			}
			else {
				$("#button-back").click();
				return;
			}

			// Mark the article as read
			if(window.articles.at(i).attributes.status == "new") {
				window.apis.at(api).article_unread(this.article, function(success) {
					if(success) {
						// Change the status of the article in the collection
						window.articles.models[i].attributes.status = "";
						window.articles.save();

						// Refresh the views
						window.router.render();
					}
				});
			}

		}
	});

	return ArticleView;
});
