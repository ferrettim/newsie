// Filename: FeedsCollection.js

define([
	'backbone', 'models/Feed'
], function(Backbone, FeedModel) {
	var FeedsCollection = Backbone.Collection.extend({
		model: FeedModel,
		unread_total: 0,

		// Order by unread items (desc)
		comparator: function(feed) {
			if($.localStorage("byunread"))
				return -feed.attributes.unread;
			else
				return feed.attributes.title;
		},
	});

	return FeedsCollection;
});
