// Filename: APIsCollection.js

define([
	'backbone', 'localStorage',
	'models/API', 'models/APINextCloud'
], function(Backbone, localStorage, APIModel, APINextCloudModel) {
	var APIsCollection = Backbone.Collection.extend({
		localStorage: new Backbone.LocalStorage("APIs"),

		// Reload the good API models
		model: function(attrs, options) {
			if(attrs.short_name == "nextcloud")
				return new APINextCloudModel(attrs, options);
			else
				return new APIModel(attrs, options);
		},

		// Save all the API models
		save: function() {
			this.each(function (api) {
				api.save();
			});
			return this;
		}
	});

	return APIsCollection;
});
