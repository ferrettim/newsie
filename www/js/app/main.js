// Filename: main.js
// The main app stuff

define([
	'backbone', 'app/router', 'views/Status', 'collections/APIs', 'collections/Articles'
], function(Backbone, Router, StatusView, APIsCollection, ArticlesCollection) {
	var initialize = function() {
		// Configuration
		window.refresh_interval = 3600;
		window.articles_per_page = 200;

		// Default values
		window.autorefresh_cnt = 0;
		window.onlyunread_cnt = 0;
		window.onlyunreadfeeds_cnt = 0;
		window.byunread_cnt = 0;
		window.darkmode_cnt = 0;
		window.lowdata_cnt = 0;
		window.autodark_cnt = 0;
		window.nightlight_cnt = 0;
		window.linknewwindow_cnt = 0;
		window.statusShown = false;
		$.ajaxSettings.timeout = 30000;	// Timeout of 30s

		if(!$.localStorage("feed"))
			$.localStorage("feed", null);

			// Check for dark model
			if($.localStorage("darkmode")) {
				$('body').css({
					'background': 'none',
					'color': 'white'
				});
				$('#article-load').css({
					'background-color': '#2d3436',
					'color': 'white'
				});
				$('#feed-load').css({
					'background-color': '#2d3436',
					'color': 'white'
				});
				$('#server').css({
					'color': 'white !important'
				});
				$('#server-address').css({
					'color': 'white !important'
				});
				$('#server-user').css({
					'color': 'white !important'
				});
				$("img .msg-image").attr('src', 'css/images/ui/checkbox2.png');
			} else {
				$('body').css({
					'background': 'none',
					'color': '#000'
				});
				$('#article-load').css({
					'background-color': '#fff',
					'color': '#000'
				});
				$('#feed-load').css({
					'background-color': '#fff',
					'color': '#000'
				});
				$('#server').css({
					'color': 'black !important'
				});
				$('#server-address').css({
					'color': 'black !important'
				});
				$('#server-user').css({
					'color': 'black !important'
				});
				$("img .msg-image").attr('src', 'css/images/ui/checkbox.png');
			}

			if($.localStorage("nightlight")) {
				if(((new Date().getHours()) <= 5) || ((new Date().getHours()) >= 18)) {
					$('#redshift').css({
						'visibility': 'visible'
					});
				}
			}

			if($.localStorage("autodark")) {
				if(((new Date().getHours()) <= 5) || ((new Date().getHours()) >= 18)) {
					$('body').css({
						'background-color': '#2d3436',
						'color': 'white'
					});
					$('#article-load').css({
						'background-color': '#2d3436',
						'color': 'white'
					});
					$('#feed-load').css({
						'background-color': '#2d3436',
						'color': 'white'
					});
					$('#server').css({
						'color': 'white !important'
					});
					$('#server-address').css({
						'color': 'white !important'
					});
					$('#server-user').css({
						'color': 'white !important'
					});
				} else {
					$('body').css({
						'background-color': '#fff',
						'color': '#000'
					});
					$('#article-load').css({
						'background-color': '#fff',
						'color': '#000'
					});
					$('#feed-load').css({
						'background-color': '#fff',
						'color': '#000'
					});
					$('#server').css({
						'color': 'black !important'
					});
					$('#server-address').css({
						'color': 'black !important'
					});
					$('#server-user').css({
						'color': 'black !important'
					});
				}
			}

		window.apis = new APIsCollection();
		window.apis.fetch();

		window.articles = new ArticlesCollection();
		window.articles.fetch();

		// Create the Router
		window.router = new Router();
	}

	return {
		initialize: initialize
	};
});
